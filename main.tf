provider "aws" {
  access_key = "${file("./provider-data/token")}"
  secret_key = "${file("./provider-data/secret")}"
  region     = "eu-central-1"
}


resource "aws_instance" "host01" {
  ami                    = var.ami
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.web-group.id]

  tags = {
    Name  = "Web Server Security Group"
    Owner = "Georgii Traksel"
  }
}

resource "aws_instance" "host02" {
  ami                    = var.ami
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.web-group.id]
  
  tags = {
    Name  = "Web Server Security Group"
    Owner = "Georgii Traksel"
  }

}

resource "aws_security_group" "web-group" {
  name = "webserver security group"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Web Server Security Group"
    Owner = "Georgii Traksel"
  }
}
